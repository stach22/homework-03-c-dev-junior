﻿using System;
using System.Collections.Generic;
using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Services;
using Homework02.CliLayer.IoHelpers;

namespace Homework02.CliLayer
{
    public class ProgramLoop
    {
        public enum CommandTypes
        {
            C0,
            C1,
            C2,
            C3,
            C4,
            C5,
            C6,
            Q
        }

        public enum CommandTypesChangeInfo
        {
            C0,
            C1,
            C2,
            Q
        }

        public void Work()
        {
            bool working = true;

            while (working)
            {
                var command = ConsoleReadHelper.GetCommandType();

                switch (command)
                {
                    case CommandTypes.C1:
                        AddCourse();
                        break;

                    case CommandTypes.C2:
                        AddStudent();
                        break;

                    case CommandTypes.C3:
                        AddCourseDay();
                        break;

                    case CommandTypes.C4:
                        AddHomework();
                        break;

                    case CommandTypes.C5:
                        ShowReport();
                        break;

                    case CommandTypes.C6:
                    {
                        var changeInfoCommand = ConsoleReadHelper.GetChangeInfoCommandType();

                        switch (changeInfoCommand)
                        {
                            case CommandTypesChangeInfo.C1:
                                ChangeStudentInfo();
                                break;

                            case CommandTypesChangeInfo.C2:
                                ChangeCourseInfo();
                                break;

                            case CommandTypesChangeInfo.Q:
                                return;
                        }
                        break;
                    }
                    case CommandTypes.Q:
                        return;
                }
            }
        }

        private void ShowReport()
        {
            var reportService = new ReportServiceProxy();
            var courseName = ConsoleReadHelper.GetExistingCourseName();
            Console.WriteLine(reportService.GetReport(courseName));
        }

        private void AddCourse()
        {
            var course = new CourseDto();

            course.CourseName = ConsoleReadHelper.GetNewCourseName();
            Console.WriteLine("Enter teacher name: ");
            course.TeacherName = ConsoleReadHelper.GetNonEmptyInput();
            course.StartTime = ConsoleReadHelper.GetStartDateTime();
            Console.WriteLine("Set min value of homeworks completed (in percent): ");
            course.HomeworkRequired = ConsoleReadHelper.GetReqInt();
            Console.WriteLine("Set min value of presence required (in percent): ");
            course.PresenceRequired = ConsoleReadHelper.GetReqInt();
            Console.WriteLine("How many students you want to add?: ");
            course.NumberOfStudents = ConsoleReadHelper.GetInt();

            var studentPesels = new List<long>();

            var studentService = new StudentService();
            for (int i = 1; i <= course.NumberOfStudents; i++)
            {
                bool studentAlreadyExists;
                do
                {
                    long studentPesel = ConsoleReadHelper.GetPesel();
                    studentAlreadyExists = studentService.CheckIfStudentExistByPesel(studentPesel);
                    if (studentAlreadyExists)
                    {
                        if (studentPesels.Contains(studentPesel))
                        {
                            Console.WriteLine("There's already student in course with this PESEL. Try Again...");
                            studentAlreadyExists = false;
                        }
                        else
                        {
                            studentPesels.Add(studentPesel);
                        }
                    }
                    else
                    {
                        Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                    }

                } while (!studentAlreadyExists);
            }

            var isCorrect = CourseService.CreateCourseWithStudents(course, studentPesels);

            ConsoleWriteHelper.OperationSuccessMessage(isCorrect);
        }
        
        private void AddStudent()
        {
            var student = new StudentDto();
            var studentService = new StudentService();
            bool studentAlreadyExists;
            do
            {
                student.Pesel = ConsoleReadHelper.GetPesel();
                studentAlreadyExists = studentService.CheckIfStudentExistByPesel(student.Pesel);
                if (studentAlreadyExists)
                {
                    Console.WriteLine("Student already exists in DataBase!");
                }
            } while (studentAlreadyExists);

            Console.WriteLine("Enter Name: ");
            student.Name = ConsoleReadHelper.GetNonEmptyInput();
            Console.WriteLine("Enter Surname: ");
            student.Surname = ConsoleReadHelper.GetNonEmptyInput();
            student.Sex = ConsoleReadHelper.GetSex();
            student.BirthDate = ConsoleReadHelper.GetBirthDateTime();
            
            var isCorrect = studentService.CreateStudent(student);
            
            ConsoleWriteHelper.OperationSuccessMessage(isCorrect);
        }

        private void AddCourseDay()
        {
            var courseDay = new CourseDayDto();

            var courseService = new CourseService();

            var courseName = ConsoleReadHelper.GetExistingCourseName();

            courseDay.CourseDayDate = ConsoleReadHelper.GetCourseDayDateTime();

            var studentDtos = courseService.GetStudentDtos(courseName);

            var presenceDtos = new List<PresenceDto>();
            foreach (var studentDto in studentDtos)
            {
                Console.WriteLine(studentDto.Name + " " + studentDto.Surname);

                PresenceDto.PresenceEnum presenceEnum;
                string presenceTyped = null;
                do
                {
                    if (presenceTyped != null)
                    {
                        Console.WriteLine("Wrong input value! Please type <Present> or <Absent>.");
                    }
                    Console.WriteLine("mark presence (Present/Absent): ");
                    presenceTyped = Console.ReadLine();
                } while (Enum.TryParse(presenceTyped, out presenceEnum) == false);

                var presence = new PresenceDto();
                presence.Pesel = studentDto.Pesel;
                presence.Value = presenceEnum;
                presenceDtos.Add(presence);
            }

            var isCorrect = CourseDayService.CreateCourseDay(courseDay, courseName, presenceDtos);

            ConsoleWriteHelper.OperationSuccessMessage(isCorrect);
        }

        private void AddHomework()
        {
            var homework = new HomeworkDto();
            var courseService = new CourseService();
            var courseName = ConsoleReadHelper.GetExistingCourseName();

            Console.WriteLine("Enter homework name: ");
            homework.HomeworkName = ConsoleReadHelper.GetNonEmptyInput();
            Console.WriteLine("Enter max points of homework: ");
            homework.MaxPoints = ConsoleReadHelper.GetInt();

            var studentDtos = courseService.GetStudentDtos(courseName);
            
            var results = new List<ResultDto>();
            foreach (var studentDto in studentDtos)
            {
                Console.WriteLine(studentDto.Name + " " + studentDto.Surname);

                int homeworkResult;
                string studentResult = null;
                bool parseSuccessful;
                do
                {
                    if (studentResult != null)
                    {
                        Console.WriteLine(
                            "Wrong input value! Please type a numeric value less than max points of homework.");
                    }
                    Console.WriteLine("Enter student's points: ");

                    studentResult = Console.ReadLine();
                    
                    parseSuccessful = Int32.TryParse(studentResult, out homeworkResult);
                    if (homeworkResult > homework.MaxPoints || homeworkResult < 0)
                    {
                        parseSuccessful = false;
                    }
                } while (!parseSuccessful);

                ResultDto result = new ResultDto();
                result.Pesel = studentDto.Pesel;
                result.Value = homeworkResult;
                results.Add(result);
            }

            var isCorrect = HomeworkService.CreateHomework(homework, courseName, results);

            ConsoleWriteHelper.OperationSuccessMessage(isCorrect);
        }

        private void ChangeStudentInfo()
        {
            var studentService = new StudentService();
            bool studentAlreadyExists;
            long pesel;
            var courseDto = new CourseDto();

            do
            {
                pesel = ConsoleReadHelper.GetPesel();

                studentAlreadyExists = studentService.CheckIfStudentExistByPesel(pesel);
                if (!studentAlreadyExists)
                {
                    Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                }
            } while (!studentAlreadyExists);

            var studentDto = studentService.GetStudentDataByPesel(pesel);

            Console.WriteLine(ConsoleWriteHelper.StudentCurrentData(studentDto) + "\n");

            do
            {
                Console.WriteLine("Choose action:\n<1> Change name\n<2> Change surname" +
                                  "\n<3> Change sex\n<4> Change date of birth\n<5> Sign out from course");

                var userChoose = ConsoleReadHelper.GetChangeDecision();

                if (userChoose == "1")
                {
                    Console.WriteLine("Enter new name:");
                    studentDto.Name = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "2")
                {
                    Console.WriteLine("Enter new surname");
                    studentDto.Surname = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "3")
                {
                    Console.WriteLine("On your own risk!");
                    studentDto.Sex = ConsoleReadHelper.GetSex();
                }
                if (userChoose == "4")
                {
                    studentDto.BirthDate = ConsoleReadHelper.GetBirthDateTime();
                }
                if (userChoose == "5")
                {
                    courseDto.CourseName = ConsoleReadHelper.GetExistingCourseName();
                    studentService.RemoveStudentFromCourse(studentDto, courseDto.CourseName);
                }

                var isCorrect = studentService.UpdateStudent(studentDto);
                ConsoleWriteHelper.OperationSuccessMessage(isCorrect);

                Console.WriteLine(ConsoleWriteHelper.StudentCurrentData(studentDto) + "\n");

                Console.WriteLine("Press any key to change other student's data, or press <Q> for back to main menu");

            } while (Console.ReadLine() != "Q");
        }

        private void ChangeCourseInfo()
        {
            var courseService = new CourseService();
            var courseDto = new CourseDto();
            courseDto.CourseName = ConsoleReadHelper.GetExistingCourseName();
            courseDto = courseService.GetCourseDataByName(courseDto.CourseName);
            var studentService = new StudentService();

            Console.WriteLine(ConsoleWriteHelper.CourseCurrentData(courseDto) + "\n");

            do
            {
                Console.WriteLine("Choose action:\n<1> Change course name\n<2> Change teacher name" +
                                  "\n<3> Change min homework % required\n<4> Change min presence % required" +
                                  "\n<5> Sign out student from course");

                var userChoose = ConsoleReadHelper.GetChangeDecision();

                if (userChoose == "1")
                {
                    courseDto.CourseName = ConsoleReadHelper.GetNewCourseName();
                }
                if (userChoose == "2")
                {
                    Console.WriteLine("Enter new teacher name");
                    courseDto.TeacherName = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "3")
                {
                    Console.WriteLine("Enter new min homework % required");
                    courseDto.HomeworkRequired = ConsoleReadHelper.GetReqInt();
                }
                if (userChoose == "4")
                {
                    Console.WriteLine("Enter new min presence % required");
                    courseDto.PresenceRequired = ConsoleReadHelper.GetReqInt();
                }
                if (userChoose == "5")
                {
                    bool studentAlreadyExists;
                    var pesel = ConsoleReadHelper.GetPesel();

                    do
                    {
                        studentAlreadyExists = studentService.CheckIfStudentExistByPesel(pesel);
                        if (!studentAlreadyExists)
                        {
                            Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                        }
                    } while (!studentAlreadyExists);
                    var studentDto = studentService.GetStudentDataByPesel(pesel);
                    studentService.RemoveStudentFromCourse(studentDto, courseDto.CourseName);
                }

                var isCorrect = courseService.UpdateCourse(courseDto);
                ConsoleWriteHelper.OperationSuccessMessage(isCorrect);

                Console.WriteLine(ConsoleWriteHelper.CourseCurrentData(courseDto) + "\n");

                Console.WriteLine("Press any key to change other course data, or press <Q> for back to main menu");

            } while (Console.ReadLine() != "Q");

        }
    }
}
