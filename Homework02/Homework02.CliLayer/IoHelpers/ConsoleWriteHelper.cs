﻿using System;
using Homework02.BusinessLayer.Dtos;

namespace Homework02.CliLayer.IoHelpers
{
    public class ConsoleWriteHelper
    {
        public static void OperationSuccessMessage(bool success)
        {
            if (success)
            {
                Console.WriteLine("Operation succeeded");
            }
            else
            {
                Console.WriteLine("Operation failed");
            }
        }

        public static string StudentCurrentData(StudentDto studentDto)
        {
            return "\nStudent's current info:\nName: " + studentDto.Name + "\nSurname: " +
                              studentDto.Surname + "\nSex: " + studentDto.Sex + "\nBirth Date: " + studentDto.BirthDate;
        }

        public static string CourseCurrentData(CourseDto courseDto)
        {
            return "\nCourse current info:\nCourse name: " + courseDto.CourseName + "\nTeacher name: " +
                   courseDto.TeacherName + "\nMin Homeworks % required: " + courseDto.HomeworkRequired 
                   + "\nMin Presence % required: " + courseDto.PresenceRequired;
        }
    }
}
