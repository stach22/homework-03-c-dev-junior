﻿using System;
using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Mappers;
using Homework02.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Homework02.UnitTests
{
    [TestClass]
    public class DtoToModelMapperTests
    {
        [TestMethod]
        public void StudentMapping_ProvideValidStudentDto_ReceiveMappedStudent()
        {
            var studentToMap = new StudentDto();
            var expectedResult = new Student();

            studentToMap.Id = 2;
            studentToMap.Pesel = 83838383838;
            studentToMap.Name = "Stefan";
            studentToMap.Surname = "Malinowski";
            studentToMap.Sex = "M";
            studentToMap.BirthDate = DateTime.Parse("1992-07-02");

            expectedResult.Id = 2;
            expectedResult.Pesel = 83838383838;
            expectedResult.Name = "Stefan";
            expectedResult.Surname = "Malinowski";
            expectedResult.Sex = "M";
            expectedResult.BirthDate = DateTime.Parse("1992-07-02");

            var studentMapped = DtoToModelMapper.StudentDtoToModelMapper(studentToMap);

            Assert.AreEqual(expectedResult, studentMapped);
        }

        [TestMethod]
        public void CourseMapping_ProvideValidCourseDto_ReceiveMappedCourse()
        {
            var courseToMap = new CourseDto();
            var expectedResult = new Course();

            courseToMap.Id = 3;
            courseToMap.CourseName = "Java WebOps";
            courseToMap.TeacherName = "Pankracy Masztalerz";
            courseToMap.StartTime = DateTime.Parse("2017-06-21");
            courseToMap.HomeworkRequired = 45;
            courseToMap.PresenceRequired = 30;

            expectedResult.Id = 3;
            expectedResult.CourseName = "Java WebOps";
            expectedResult.TeacherName = "Pankracy Masztalerz";
            expectedResult.StartTime = DateTime.Parse("2017-06-21");
            expectedResult.HomeworkRequired = 45;
            expectedResult.PresenceRequired = 30;

            var courseMapped = DtoToModelMapper.CourseDtoToModelMapper(courseToMap);

            Assert.AreEqual(expectedResult, courseMapped);
        }
    }
}
