﻿using System.Linq;
using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class ResultRepo
    {
        public static bool AddResultDb(Result result)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.StudentDbSet.Attach(result.Student);
                dbContext.HomeworkDbSet.Attach(result.Homework);
                dbContext.ResultDbSet.Add(result);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public virtual int GetPointsSumForStudentInCourse(Student student, Course course)
        {
            var pointsSum = 0;

            using (var dbContext = new Homework02DbContext())
            {
                var resultDbSet = dbContext.ResultDbSet;

                if (resultDbSet.Any())
                {
                    pointsSum = resultDbSet
                        .Where(r => r.Student.Id == student.Id)
                        .Where(r => r.Homework.Course.Id == course.Id)
                        .Sum(r => r.Value);
                }
            }

            return pointsSum;
        }
    }
}