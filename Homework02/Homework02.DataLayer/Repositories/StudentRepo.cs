﻿using System.Data.Entity;
using System.Linq;
using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class StudentRepo
    {
        public static bool AddStudentDb(Student student)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.StudentDbSet.Add(student);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public static Student GetStudentByPesel(long pesel)
        {
            using (var dbContext = new Homework02DbContext())
            {
                return dbContext.StudentDbSet.SingleOrDefault(b => b.Pesel == pesel);
            }
        }

        public static bool UpdateStudentDb(Student student)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.StudentDbSet.Attach(student);
                dbContext.Entry(student).State = EntityState.Modified;
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public static Student GetStudentData(long pesel)
        {
            using (var dbContext = new Homework02DbContext())
            {
                var studentByPesel = dbContext.StudentDbSet
                    .Include("Courses")
                    .SingleOrDefault(s => s.Pesel == pesel);

                return studentByPesel;
            }
        }

    }
}