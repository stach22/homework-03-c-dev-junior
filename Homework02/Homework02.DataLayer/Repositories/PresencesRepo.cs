﻿using System.Linq;
using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class PresencesRepo
    {
        public static bool AddPresenceDb(Presence presence)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.StudentDbSet.Attach(presence.Student);
                dbContext.CourseDayDbSet.Attach(presence.CourseDay);
                dbContext.PresencesDbSet.Add(presence);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public virtual int GetPresenceForStudentInCourse(Student student, Course course)
        {
            var presences = 0;
            using (var dbContext = new Homework02DbContext())
            {
                var presenceDbSet = dbContext.PresencesDbSet;
                if (presenceDbSet.Any())
                {
                    presences = dbContext.PresencesDbSet
                        .Where(p => p.Student.Id == student.Id)
                        .Where(p => p.CourseDay.Course.Id == course.Id)
                        .Count(p => p.Value == Presence.PresenceEnum.Present);
                }
            }

            return presences;
        }
    }
}