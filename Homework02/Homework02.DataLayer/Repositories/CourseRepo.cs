﻿using System.Data.Entity;
using System.Linq;
using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class CourseRepo
    {
        public static bool AddCourseDb(Course course)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                foreach (var student in course.Students)
                {
                    dbContext.StudentDbSet.Attach(student);
                }

                dbContext.CourseDbSet.Add(course);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public virtual Course GetCourseByName(string courseName)
        {
            using (var dbContext = new Homework02DbContext())
            {
                var courseByName = dbContext.CourseDbSet
                    .Include("CourseDays")
                    .Include("Homeworks")
                    .Include("Students")
                    .SingleOrDefault(b => b.CourseName == courseName);

                return courseByName;
            }
        }

        public static bool RemoveStudentFromCourse(Course course, Student student)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                var studentDb = (from s in dbContext.StudentDbSet where s.Id == student.Id select s).FirstOrDefault();
                var courseDb = (from c in dbContext.CourseDbSet where c.Id == course.Id select c).FirstOrDefault();

                studentDb.Courses.Remove(courseDb);
                courseDb.Students.Remove(studentDb);

                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public static bool UpdateCourseDb(Course course)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.CourseDbSet.Attach(course);
                dbContext.Entry(course).State = EntityState.Modified;
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

    }
}