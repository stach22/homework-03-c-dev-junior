﻿using System.Linq;
using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class HomeworkRepo
    {
        public static bool AddHomeworkDb(Homework homework)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.CourseDbSet.Attach(homework.Course);
                dbContext.HomeworkDbSet.Add(homework);

                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }

        public virtual int GetMaxPointsSumInCourse(Course course)
        {
            var maxPointsSum = 0;

            using (var dbContext = new Homework02DbContext())
            {
                var homeworkDbSet = dbContext.HomeworkDbSet;
                if (homeworkDbSet.Any())
                {
                    maxPointsSum = homeworkDbSet
                        .Where(h => h.Course.Id == course.Id)
                        .Sum(h => h.MaxPoints);
                }
            }

            return maxPointsSum;
        }
    }
}