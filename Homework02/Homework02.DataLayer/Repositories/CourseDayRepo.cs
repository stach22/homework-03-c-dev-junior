﻿using Homework02.DataLayer.DbContexts;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.Repositories
{
    public class CourseDayRepo
    {
        public static bool AddCourseDayDb(CourseDay courseDay)
        {
            int rowsAffected;

            using (var dbContext = new Homework02DbContext())
            {
                dbContext.CourseDbSet.Attach(courseDay.Course);
                dbContext.CourseDayDbSet.Add(courseDay);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected != 0;
        }
    }
}
