﻿using System.Collections.Generic;

namespace Homework02.DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public string HomeworkName { get; set; }
        public int MaxPoints { get; set; }

        public virtual Course Course { get; set; }
        public virtual List<Result> Results { get; set; } = new List<Result>();
    }
}
