﻿using System;
using System.Collections.Generic;

namespace Homework02.DataLayer.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public string TeacherName { get; set; }
        public DateTime StartTime { get; set; }
        public int HomeworkRequired { get; set; }
        public int PresenceRequired { get; set; }

        public virtual List<Student> Students { get; set; }
        public virtual List<CourseDay> CourseDays { get; set; } = new List<CourseDay>();
        public virtual List<Homework> Homeworks { get; set; } = new List<Homework>();

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var course = obj as Course;

            if (course == null)
            {
                return false;
            }

            bool areEqual = true;
            areEqual &= Id == course.Id;
            areEqual &= CourseName == course.CourseName;
            areEqual &= TeacherName == course.TeacherName;
            areEqual &= StartTime == course.StartTime;
            areEqual &= HomeworkRequired == course.HomeworkRequired;
            areEqual &= PresenceRequired == course.PresenceRequired;

            return areEqual;
        }

    }
}