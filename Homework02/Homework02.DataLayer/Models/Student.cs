﻿using System;
using System.Collections.Generic;

namespace Homework02.DataLayer.Models
{
    public class Student
    {
        public int Id { get; set; }
        public long Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Sex { get; set; }
        public DateTime BirthDate { get; set; }

        public virtual List<Course> Courses { get; set; }
        public virtual List<Presence> Presences { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var student = obj as Student;

            if (student == null)
            {
                return false;
            }

            bool areEqual = true;
            areEqual &= Id == student.Id;
            areEqual &= Pesel == student.Pesel;
            areEqual &= Name == student.Name;
            areEqual &= Surname == student.Surname;
            areEqual &= Sex == student.Sex;
            areEqual &= BirthDate == student.BirthDate;

            return areEqual;
        }

    }
}