﻿namespace Homework02.DataLayer.Models
{
    public class Presence
    {
        public enum PresenceEnum
        {
            Present,
            Absent
        }

        public int Id { get; set; }
        public virtual Student Student { get; set; }
        public PresenceEnum Value { get; set; }
        public virtual CourseDay CourseDay { get; set; }
    }
}
