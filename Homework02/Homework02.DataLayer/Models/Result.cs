﻿namespace Homework02.DataLayer.Models
{
    public class Result
    {
        public int Id { get; set; }
        public virtual Homework Homework { get; set; }
        public virtual Student Student { get; set; }
        public int Value { get; set; }
    }
}
