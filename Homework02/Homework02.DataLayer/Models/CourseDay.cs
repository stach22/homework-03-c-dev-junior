﻿using System;
using System.Collections.Generic;

namespace Homework02.DataLayer.Models
{
    public class CourseDay
    {
        public int Id { get; set; }
        public DateTime CourseDayDate { get; set; }

        public virtual Course Course { get; set; }
        public virtual List<Presence> Presences { get; set; } = new List<Presence>();
    }
}
