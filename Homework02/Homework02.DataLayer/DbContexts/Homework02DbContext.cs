﻿using System.Configuration;
using System.Data.Entity;
using Homework02.DataLayer.Models;

namespace Homework02.DataLayer.DbContexts
{
    public class Homework02DbContext : DbContext
    {
        public Homework02DbContext() : base(GetConnectionString())
        {
        }

        public DbSet<Course> CourseDbSet { get; set; }
        public DbSet<Student> StudentDbSet { get; set; }
        public DbSet<Homework> HomeworkDbSet { get; set; }
        public DbSet<Result> ResultDbSet { get; set; }
        public DbSet<CourseDay> CourseDayDbSet { get; set; }
        public DbSet<Presence> PresencesDbSet { get; set; }

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }
    }
}