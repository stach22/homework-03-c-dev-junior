﻿using Homework02.DataLayer.Models;
using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class ReportService
    {
        private HomeworkRepo _homeworkRepo;
        private ResultRepo _resultRepo;
        private PresencesRepo _presencesRepo;
        private CourseRepo _courseRepo;

        public ReportService(HomeworkRepo homeworkRepo, ResultRepo resultRepo, PresencesRepo presencesRepo, CourseRepo courseRepo)
        {
            _homeworkRepo = homeworkRepo;
            _resultRepo = resultRepo;
            _presencesRepo = presencesRepo;
            _courseRepo = courseRepo;
        }

        public virtual string StudentsPresence(Course course)
        {
            var message = "";
            foreach (var student in course.Students)
            {
                var presences = _presencesRepo.GetPresenceForStudentInCourse(student, course);

                var courseDaysCount = course.CourseDays.Count;
                var presencesPercent = Percentage(presences, courseDaysCount);
                var passed = Passed(presencesPercent, course.PresenceRequired);
                message += student.Pesel + " " + student.Name + " " + student.Surname + " "
                           + presences + "/" + courseDaysCount + " (" + presencesPercent.ToString("0.00") + "%) - " +
                           passed + "\n";
            }

            return message;
        }

        public string GetReport(string courseName)
        {
            var course = _courseRepo.GetCourseByName(courseName);

            var report =
                "Course name: " + course.CourseName + "\n" +
                "Course Teacher: " + course.TeacherName + "\n" +
                "Course start date: " + course.StartTime + "\n" +
                "Course presence required: " + course.PresenceRequired + "%" + "\n" +
                "Course homework required: " + course.HomeworkRequired + "%" + "\n" +
                "Each student's presence: " + "\n" +
                StudentsPresence(course) +
                "Each student's homeworks done: " + "\n" +
                StudentsHomeworks(course);

            return report;
        }

        public string StudentsHomeworks(Course course)
        {
            var message = "";
            foreach (var student in course.Students)
            {

                var homeworkPointsSum = _resultRepo.GetPointsSumForStudentInCourse(student, course);
                var homeworkMaxPointsSum = _homeworkRepo.GetMaxPointsSumInCourse(course);

                var homeworkPercent = Percentage(homeworkPointsSum, homeworkMaxPointsSum);
                var passed = Passed(homeworkPercent, course.HomeworkRequired);
                message += student.Pesel + " " + student.Name + " " + student.Surname + " "
                           + homeworkPointsSum + "/" + homeworkMaxPointsSum + " (" + homeworkPercent.ToString("0.00") +
                           "%) - " + passed + "\n";
            }

            return message;
        }

        public static string Passed(double percent, double threshold)
        {
            if (percent >= threshold)
            {
                return "Passed";
            }
            else
            {
                return "Failed";
            }
        }

        public static double Percentage(double a, double b)
        {
            if (b != 0)
            {
                return a / b * 100;
            }
            else
            {
                return 0;
            }
        }
    }
}