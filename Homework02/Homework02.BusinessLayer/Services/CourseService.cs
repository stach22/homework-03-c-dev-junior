﻿using System.Collections.Generic;
using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Mappers;
using Homework02.DataLayer.Models;
using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class CourseService
    {
        CourseRepo courseRepo = new CourseRepo();

        public List<StudentDto> GetStudentDtos(string courseName)
        {
            var course = courseRepo.GetCourseByName(courseName);
            var studentDtos = new List<StudentDto>();

            foreach (var student in course.Students)
            {
                studentDtos.Add(ModelToDtoMapper.ModelToStudentDto(student));
            }

            return studentDtos;
        }

        public static bool CreateCourseWithStudents(CourseDto courseDto, List<long> studentPesels)
        {
            var course = DtoToModelMapper.CourseDtoToModelMapper(courseDto);

            var students = new List<Student>();

            foreach (var pesel in studentPesels)
            {
                var student = StudentRepo.GetStudentByPesel(pesel);
                students.Add(student);
            }

            course.Students = students;

            return CourseRepo.AddCourseDb(course);
        }

        public bool CheckIfCourseExistByName(string courseName)
        {
            var course = courseRepo.GetCourseByName(courseName);

            if (course == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public CourseDto GetCourseDataByName(string courseName)
        {
            var course = courseRepo.GetCourseByName(courseName);

            var courseDto = ModelToDtoMapper.CourseModelToDtoMapper(course);

            return courseDto;
        }

        public bool UpdateCourse(CourseDto courseDto)
        {
            var course = DtoToModelMapper.CourseDtoToModelMapper(courseDto);

            return CourseRepo.UpdateCourseDb(course);
        }

    }
}