﻿using System.Collections.Generic;
using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Mappers;
using Homework02.DataLayer.Models;
using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class HomeworkService
    {
        public static bool CreateHomework(HomeworkDto homeworkDto, string courseName, List<ResultDto> resultDtos)
        {
            var courseRepo = new CourseRepo();
            var homework = DtoToModelMapper.HomeworkDtoToModelMapper(homeworkDto);
            var course = courseRepo.GetCourseByName(courseName);

            homework.Course = course;

            var results = new List<Result>();

            foreach (var student in course.Students)
            {
                var result = new Result();
                result.Value = resultDtos.Find(r => r.Pesel == student.Pesel).Value;
                result.Student = student;
                result.Homework = homework;

                results.Add(result);
            }

            var success = HomeworkRepo.AddHomeworkDb(homework);

            foreach (var result in results)
            {
                success &= ResultRepo.AddResultDb(result);
            }

            return success;
        }
    }
}