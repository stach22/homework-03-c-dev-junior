﻿using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class ReportServiceProxy
    {
        public string GetReport(string courseName)
        {
            var homeworkRepository = new HomeworkRepo();
            var resultRepository = new ResultRepo();
            var presencesRepository = new PresencesRepo();
            var courseRepository = new CourseRepo();

            var reportService = new ReportService(homeworkRepository, resultRepository, presencesRepository, courseRepository);

            return reportService.GetReport(courseName);
        }

    }
}
