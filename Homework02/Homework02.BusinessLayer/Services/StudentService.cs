﻿using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Mappers;
using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class StudentService
    {
        public bool CreateStudent(StudentDto studentDto)
        {
            var student = DtoToModelMapper.StudentDtoToModelMapper(studentDto);

            return StudentRepo.AddStudentDb(student);
        }

        public bool CheckIfStudentExistByPesel(long pesel)
        {
            var student = StudentRepo.GetStudentByPesel(pesel);

            if (student == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public StudentDto GetStudentDataByPesel(long pesel)
        {
            var student = StudentRepo.GetStudentByPesel(pesel);

            var studentDto = ModelToDtoMapper.ModelToStudentDto(student);

            return studentDto;
        }

        public bool UpdateStudent(StudentDto studentDto)
        {
            var student = DtoToModelMapper.StudentDtoToModelMapper(studentDto);

            return StudentRepo.UpdateStudentDb(student);
        }

        public bool RemoveStudentFromCourse(StudentDto studentDto, string courseName)
        {
            var courseRepo = new CourseRepo();
            var student = StudentRepo.GetStudentData(studentDto.Pesel);

            var course = courseRepo.GetCourseByName(courseName);


            return CourseRepo.RemoveStudentFromCourse(course, student);
        }
    }
}