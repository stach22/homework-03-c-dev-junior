﻿using System.Collections.Generic;
using Homework02.BusinessLayer.Dtos;
using Homework02.BusinessLayer.Mappers;
using Homework02.DataLayer.Models;
using Homework02.DataLayer.Repositories;

namespace Homework02.BusinessLayer.Services
{
    public class CourseDayService
    {
        public static bool CreateCourseDay(CourseDayDto courseDayDto, string courseName, List<PresenceDto> presenceDtos)
        {
            var courseRepo = new CourseRepo();
            var courseDay = DtoToModelMapper.CourseDayDtoToModelMapper(courseDayDto);
            var course = courseRepo.GetCourseByName(courseName);

            courseDay.Course = course;

            var presences = new List<Presence>();
            foreach (var student in course.Students)
            {
                var presenceDto = presenceDtos.Find(p => p.Pesel == student.Pesel);
                var presence = DtoToModelMapper.PresenceDtoToModelMapper(presenceDto);

                presence.Student = student;
                presence.CourseDay = courseDay;

                presences.Add(presence);
            }

            var success = true;

            success &= CourseDayRepo.AddCourseDayDb(courseDay);

            foreach (var presence in presences)
            {
                success &= PresencesRepo.AddPresenceDb(presence);
            }

            return success;
        }
    }
}