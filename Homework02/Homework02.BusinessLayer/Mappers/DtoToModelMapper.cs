﻿using Homework02.BusinessLayer.Dtos;
using Homework02.DataLayer.Models;

namespace Homework02.BusinessLayer.Mappers
{
    public class DtoToModelMapper
    {
        public static Student StudentDtoToModelMapper(StudentDto studentDto)
        {
            var student = new Student();

            student.Id = studentDto.Id;
            student.Pesel = studentDto.Pesel;
            student.Name = studentDto.Name;
            student.Surname = studentDto.Surname;
            student.Sex = studentDto.Sex;
            student.BirthDate = studentDto.BirthDate;

            return student;
        }

        public static Course CourseDtoToModelMapper(CourseDto courseDto)
        {
            var course = new Course();

            course.Id = courseDto.Id;
            course.CourseName = courseDto.CourseName;
            course.TeacherName = courseDto.TeacherName;
            course.StartTime = courseDto.StartTime;
            course.HomeworkRequired = courseDto.HomeworkRequired;
            course.PresenceRequired = courseDto.PresenceRequired;

            return course;
        }

        public static CourseDay CourseDayDtoToModelMapper(CourseDayDto courseDayDto)
        {
            var courseDay = new CourseDay();

            courseDay.CourseDayDate = courseDayDto.CourseDayDate;

            return courseDay;
        }

        public static Homework HomeworkDtoToModelMapper(HomeworkDto homeworkDto)
        {
            var homework = new Homework();

            homework.HomeworkName = homeworkDto.HomeworkName;
            homework.MaxPoints = homeworkDto.MaxPoints;

            return homework;
        }

        public static Presence PresenceDtoToModelMapper(PresenceDto presenceDto)
        {
            var presence = new Presence();

            presence.Value = (Presence.PresenceEnum) presenceDto.Value;

            return presence;
        }
    }
}
