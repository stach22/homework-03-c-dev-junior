﻿namespace Homework02.BusinessLayer.Dtos
{
    public class PresenceDto
    {
        public enum PresenceEnum
        {
            Present,
            Absent
        }

        public long Pesel;
        public PresenceEnum Value;
    }
}