﻿namespace Homework02.BusinessLayer.Dtos
{
    public class HomeworkDto
    {
        public string HomeworkName;
        public int MaxPoints;
    }
}